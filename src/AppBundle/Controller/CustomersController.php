<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class CustomersController extends Controller {

    /**
     * @Route("/customers/")
     * @Method("GET")
     */
    public function getAction() {
        $cacheService = $this->get('cache_service');
        $key = 'customers';
        $customers = $cacheService->get($key);
        $online = $customers->online;
        if (empty($customers->data)) {
            $customers = $this->findCustomersFromMongodb();
            if ($online == true) {
                $this->loadDataOnRedis($customers, $cacheService);
            }
        } else {
            $customers = $customers->data;
        }

        return new JsonResponse($customers);
    }

    /**
     * @Route("/customers/")
     * @Method("POST")
     */
    public function postAction(Request $request) {
        $database = $this->get('database_service')->getDatabase();
        $customers = json_decode($request->getContent());

        if (empty($customers)) {
            return new JsonResponse(['status' => 'No donuts for you'], 400);
        }

        foreach ($customers as $customer) {
            $database->customers->insert($customer);
        }
        $cacheService = $this->get('cache_service');
        $this->loadDataOnRedis($this->findCustomersFromMongodb(), $cacheService);
        return new JsonResponse(['status' => 'Customers successfully created']);
    }

    /**
     * @Route("/customers/")
     * @Method("DELETE")
     */
    public function deleteAction() 
    {
        $database = $this->get('database_service')->getDatabase();
        $database->customers->drop();
        $cacheService = $this->get('cache_service');
        $cacheService->del('customers');
        return new JsonResponse(['status' => 'Customers successfully deleted']);
    }

    /**
     * This method goes through the customer array and load each one on a redis
     * List
     */

    private function loadDataOnRedis($customers, $cacheService) 
    {
        if ($cacheService->validateRedisConnection()) {
            foreach ($customers as $record) {
                $cacheService->set("customers", json_encode($record));
            }
        }
    }

    /**
     * This method finds the customers on MongoDb
     * @return object list with the customers
     */
    
    private function findCustomersFromMongodb() 
    {
        $database = $this->get('database_service')->getDatabase();
        $customers = $database->customers->find();
        $customers = iterator_to_array($customers);
        return $customers;
    }
}
